Simple HTML Game Template
=========================

This is intended to be a basic starting point for anyone who would like to make
their own 2D games, but doesn't know where to begin.

A basic knowledge of Javascript is required, so complete beginners should check
out the guides and tutorials on the Mozilla Developer Network first:

https://developer.mozilla.org/en-US/docs/Web/JavaScript

However, I've made an effort to keep things simple, so feel free to dive in and
learn things as you go, if that's what you'd prefer.

### What you'll need

You don't need much in the way of programs:

+ To view the game, open `index.html` in a web browser like Chrome or Firefox.
+ To edit the code, open `index.html` in a text editor like Notepad++ or Kate.

You'll also need a gamepad (such as an XBox 360 controller) to actually play!

### Adding your own images

To add new images to the game, just add them to the HTML:

    <div style="display:none">
      <img id="player" src="player.png">
      <img id="pizza" src="pizza.png">
    </div>

...and then set them up in your Javascript code:

    let player = document.getElementById('player')
    let pizza = document.getElementById('pizza')

Any of the usual web image formats (JPEG, PNG, GIF) will work, but animated GIF
files will only display their first frame of animation.

Limitations
-----------

In order to be as simple as possible, a few things have been omitted.

### Only gamepads can be used.

Keyboard (or mouse, or touch-screen) input would require additional code, so to
keep things relatively easy-to-understand I have opted to only use gamepads.

Of course, you _can_ add all of these things yourself, if you feel adventurous;
this code is just meant to be a starting point for you to build on, after all!

### No audio.

This template originally included sound effects. However, modern browsers block
auto-play audio by default (unless the user explicitly does something to enable
it, like click a button), and so this was removed in order to avoid extra code.

Hints and Tips
--------------

Search MDN Web Docs if you get stuck. Some useful pages:

+ https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D
+ https://developer.mozilla.org/en-US/docs/Web/API/Gamepad

To see errors in your browser, open the Developer Tools (F12) and switch to the
**Console** tab. This will help you figure out what's broken if your code isn't
working.

### Animated sprites

As mentioned above, .GIF files do not animate when drawn on a HTML canvas. This
is just how HTML canvas elements work, and not an intentional limitation of the
code provided here.

If you want animated sprites, you could simply use multiple image files, or use
a more-complex version of `ctx.drawImage()` with a single "sprite sheet" image:

https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage

The former is probably easier if you don't want to make your code more complex,
so you might want to choose this option if you're new to coding in general.

### Gamepads in Firefox

Firefox doesn't support the "standardised" gamepad layout at the moment:

https://bugzilla.mozilla.org/show_bug.cgi?id=855364

As such, you may find that the buttons/axes are "switched around" on some pads.
However, axes 0/1 are almost always used for the left stick and buttons 0/1 are
almost always used for what would be `X` and `O` on a PlayStation pad.

Chrome _does_ use a standardised layout, so common pads will behave identically
in terms of axis/button numbering. If you're running into trouble with Firefox,
try using Chrome instead.

Licence
-------

Copyright (C) 2020 by Thomas Dennis <go.busto@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
